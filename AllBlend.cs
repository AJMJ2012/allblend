﻿using Terraria.ID;
using Terraria.ModLoader;
using Terraria;
using System.Collections.Generic;
using System.Linq;

namespace AllBlend {
	public class AllBlend : Mod {
		public AllBlend() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}

		public override void Load() {
			DALib.ConflictCheck.AddConflict(this, "NoBlend");
		}
		
		public static List<int> Exclude = new List<int> {
			TileID.Teleporter,
			TileID.SillyBalloonPink,
			TileID.SillyBalloonPurple,
			TileID.SillyBalloonGreen,
		};

		public bool IsMergeable(int tileID) {
			return  !Exclude.Any(s => s == (tileID)) && Main.tileSolid[tileID] && !Main.tileSolidTop[tileID] && !Main.tileNoAttach[tileID];
		}

		public bool MergeTo(int tileID) {
			return IsMergeable(tileID) && !TileID.Sets.TeamTiles[tileID] && !TileID.Sets.Conversion.Grass[tileID] && !TileID.Sets.GrassSpecial[tileID];
		}

		public override void PostAddRecipes() {
			if (Main.netMode == 2) { return; }
			for (int index = 0; index < TileLoader.TileCount; ++index) {
				if (Main.tileBlendAll[index] || Main.tileMergeDirt[index] || index == TileID.Dirt) {
					Main.tileBlendAll[index] = false;
					Main.tileMergeDirt[index] = false;
					Main.tileBrick[index] = true;
				}
				for (int index2 = 0; index2 < TileLoader.TileCount; ++index2) {
					Main.tileMerge[index][index2] = false;
					if ((MergeTo(index) && IsMergeable(index2)) || (Main.tileRope[index] && (Main.tileRope[index2] || IsMergeable(index2)))) {
						Main.tileMerge[index][index2] = true;
					}
				}
			}
		}
	}
}